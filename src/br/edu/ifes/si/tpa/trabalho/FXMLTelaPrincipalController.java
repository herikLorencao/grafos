/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho;

import br.edu.ifes.si.tpa.trabalho.algoritmos.Algoritmo;
import br.edu.ifes.si.tpa.trabalho.desenho.Desenho;
import br.edu.ifes.si.tpa.trabalho.model.Caminho;
import br.edu.ifes.si.tpa.trabalho.model.Vertice;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Rafael Vargas
 */
public class FXMLTelaPrincipalController implements Initializable {

    @FXML
    private AnchorPane anchorPaneGrafo;
    @FXML
    private Button buttonMostrarCaminhos;
    @FXML
    private TextField textKmMaxima;
    @FXML
    private TableView<Caminho> tableViewCaminhos;
    @FXML
    private TableColumn<Caminho, Double> tableColumnCaminhoIndice;
    @FXML
    private TableColumn<Caminho, Double> tableColumnCaminhoKM;
    @FXML
    private TableColumn<Caminho, Double> tableColumnCaminhoIPH;
    @FXML
    private TableColumn<Caminho, Double> tableColumnCaminhoPopulacao;
    @FXML
    private TableColumn<Caminho, String> tableColumnCaminhoArestas;

    private ObservableList<Caminho> observableListCaminhos;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Desenho desenho = new Desenho(anchorPaneGrafo);
        desenho.exibirGrafo();
    }

    @FXML
    private void handleButtonMostrarCaminhos(ActionEvent event) {
        List<Caminho> listCaminhos = new ArrayList<>();

        double distancia = this.getDistanciaMaxima();

        for (Vertice vertice : Main.G.getListVertices()) {
            Algoritmo algoritmo = new Algoritmo(Main.G, vertice.getId(), distancia);
            listCaminhos.addAll(algoritmo.getListCaminhos());
        }

        this.montarTabela(listCaminhos);
    }

    private double getDistanciaMaxima() {
        double distancia = 0;

        try {
            distancia = Double.valueOf(textKmMaxima.getText());
        } catch (NumberFormatException e) {
            this.mostrarAlerta("Distância inválida", "Informe um valor númerico válido!");
        }

        return distancia;
    }

    private void montarTabela(List<Caminho> listCaminhos) {
        this.definirColunasTabela();
        observableListCaminhos = FXCollections.observableArrayList(listCaminhos);
        tableViewCaminhos.setItems(observableListCaminhos);
    }

    private void definirColunasTabela() {
        tableColumnCaminhoIndice.setCellValueFactory(new PropertyValueFactory<>("indice"));
        tableColumnCaminhoKM.setCellValueFactory(new PropertyValueFactory<>("distanciaAtual"));
        tableColumnCaminhoIPH.setCellValueFactory(new PropertyValueFactory<>("iph"));
        tableColumnCaminhoPopulacao.setCellValueFactory(new PropertyValueFactory<>("populacao"));
        tableColumnCaminhoArestas.setCellValueFactory(new PropertyValueFactory<>("caminhoExibido"));
    }

    private void mostrarAlerta(String titulo, String conteudo) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(titulo);
        alert.setContentText(conteudo);
        alert.show();
    }

}

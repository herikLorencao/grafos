/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho;

import br.edu.ifes.si.tpa.trabalho.arquivo.In;
import br.edu.ifes.si.tpa.trabalho.model.Grafo;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author java
 */
public class Main extends Application {

    public static Grafo G;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLTelaPrincipal.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("IFES - SI - TPA - Trabalho 01 - Grafos");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        In in = new In(args[0]);
        G = new Grafo(in);

        launch(args);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho.algoritmos;

import br.edu.ifes.si.tpa.trabalho.model.Caminho;
import br.edu.ifes.si.tpa.trabalho.model.Aresta;
import br.edu.ifes.si.tpa.trabalho.model.Grafo;
import br.edu.ifes.si.tpa.trabalho.model.Vertice;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author herik
 */
public class Algoritmo {

    private final boolean[] noCaminho;
    private final Pilha<Integer> caminho;
    private final List<Caminho> listCaminhos;
    private final double distanciaMaxima;

    public Algoritmo(Grafo grafo, int valorVerticeOrigem, double distanciaMaxima) {
        this.noCaminho = new boolean[grafo.V()];
        this.listCaminhos = new ArrayList<>();
        this.caminho = new Pilha<>();
        this.distanciaMaxima = distanciaMaxima;
        this.dfs(grafo, valorVerticeOrigem, true);
    }

    private void dfs(Grafo grafo, int v, boolean isInicio) {
        this.caminho.empilha(v);

        if (isInicio) {
            this.noCaminho[v] = false;
            isInicio = !isInicio;
        } else {
            this.noCaminho[v] = true;
        }

        this.montaCaminho(grafo);

        for (Aresta a : grafo.adj(v)) {
            int x = a.getV2().getId();

            if (!noCaminho[x]) {
                dfs(grafo, x, isInicio);
            }
        }

        caminho.desempilha();
        noCaminho[v] = false;
    }

    private void montaCaminho(Grafo grafo) {
        Pilha<Integer> pilhaInvertida = new Pilha<>();
        List<Vertice> listVertices = new ArrayList<>();

        for (int v : caminho) {
            pilhaInvertida.empilha(v);
        }

        if (pilhaInvertida.tamanho() > 1) {
            Vertice vInicial = grafo.getVerticeById(pilhaInvertida.desempilha());

            while (!pilhaInvertida.isEmpty()) {
                listVertices.add(grafo.getVerticeById(pilhaInvertida.desempilha()));
            }

            Caminho caminhoObj = new Caminho(grafo, vInicial, listVertices);

            if (caminhoObj.getDistanciaAtual() <= this.distanciaMaxima) {
                this.addCaminhoLista(caminhoObj);
            }
        }
    }

    private void addCaminhoLista(Caminho caminhoGerado) {
        this.listCaminhos.add(caminhoGerado);
    }

    public List<Caminho> getListCaminhos() {
        return this.listCaminhos;
    }
}

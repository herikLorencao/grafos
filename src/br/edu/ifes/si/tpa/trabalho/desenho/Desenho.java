/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho.desenho;

import br.edu.ifes.si.tpa.trabalho.Main;
import br.edu.ifes.si.tpa.trabalho.model.Aresta;
import br.edu.ifes.si.tpa.trabalho.model.Vertice;
import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

/**
 *
 * @author herik
 */
public class Desenho {

    AnchorPane anchorPaneGrafo;

    public Desenho(AnchorPane anchorPaneGrafo) {
        this.anchorPaneGrafo = anchorPaneGrafo;
    }

    public void exibirGrafo() {
        Group componentes = new Group();

        this.exibeAresta(componentes);
        this.exibeVertice(componentes);

        anchorPaneGrafo.getChildren().addAll(componentes);
    }

    private void exibeAresta(Group componentes) {
        for (Vertice vertice : Main.G.getListVertices()) {
            for (Aresta aresta : Main.G.adj(vertice.getId())) {
                Vertice v1 = Main.G.getVerticeById(aresta.getV1().getId());
                Vertice v2 = Main.G.getVerticeById(aresta.getV2().getId());

                Line linha = this.criarLinha(v1, v2, this.getCorAresta(aresta.getEstado()));
                componentes.getChildren().add(linha);
            }
        }

    }

    private void exibeVertice(Group componentes) {
        for (Vertice vertice : Main.G.getListVertices()) {
            Circle circle = this.criarCirculo(vertice);

            Text text = new Text(circle.getCenterX(), circle.getCenterY(), Integer.toString(vertice.getId()));
            centralizarText(text, circle);

            componentes.getChildren().add(circle);
            componentes.getChildren().add(text);
        }
    }

    private Line criarLinha(Vertice v1, Vertice v2, Color cor) {
        Line line = new Line();
        line.setStartX(v1.getX());
        line.setStartY(v1.getY());
        line.setEndX(v2.getX());
        line.setEndY(v2.getY());
        line.setStroke(cor);
        return line;
    }

    private Circle criarCirculo(Vertice v) {
        Circle circle = new Circle();
        circle.setCenterX(v.getX());
        circle.setCenterY(v.getY());
        circle.setRadius((double) v.getPopulacao() / 500);
        circle.setFill(this.getCorVertice(v.getIph()));

        return circle;
    }

    private Color getCorVertice(double iph) {
        if (iph >= 50) {
            return Color.RED;
        } else if (iph >= 30) {
            return Color.YELLOW;
        } else {
            return Color.GREEN;
        }
    }

    private Color getCorAresta(int estado) {
        switch (estado) {
            case 1:
                return Color.GREEN;
            case 2:
                return Color.YELLOW;
            case 3:
                return Color.RED;
            default:
                throw new IllegalArgumentException("Cor inválida");
        }
    }

    private void centralizarText(Text text, Circle circle) {
        double W = text.getBoundsInLocal().getWidth();
        double H = text.getBoundsInLocal().getHeight();
        text.relocate(circle.getCenterX() - W / 2, circle.getCenterY() - H / 2);
    }
}

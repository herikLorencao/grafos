/**
 * ****************************************************************************
 *  Compilação:  javac Aresta.java
 *  Execução:    java Aresta
 *
 *  Aresta Ponderada ou Não.
 *
 *****************************************************************************
 */
package br.edu.ifes.si.tpa.trabalho.model;

/**
 * Cada aresta é composta por dois números (que representam os vértices) e um
 * peso.
 */
public class Aresta implements Comparable<Aresta> {

    private Vertice v1;
    private Vertice v2;
    private int km;
    private int estado;

    /**
     * Inicializa uma aresta (sem peso) entre vértices.
     *
     * @param v1 vértice 1 (origem)
     * @param v2 vértice 2 (destino)
     * @throws IndexOutOfBoundsException se o vértice 1 ou vértice 2 forem
     * negativos
     * @throws IllegalArgumentException se o peso for um valor não numérico
     *
     */
    public Aresta(Vertice v1, Vertice v2) {
        if (v1.getId() < 0) {
            throw new IndexOutOfBoundsException("Vértice deve ser um inteiro não negativo");
        }
        if (v2.getId() < 0) {
            throw new IndexOutOfBoundsException("Vértice deve ser um inteiro não negativo");
        }
        this.v1 = v1;
        this.v2 = v2;
        this.km = 0;
        this.estado = 0;
    }

    /**
     * Inicializa uma aresta entre vértices e atribui seu peso.
     *
     * @param v1 vértice 1 (origem)
     * @param v2 vértice 2 (destino)
     * @param peso peso da aresta
     * @throws IndexOutOfBoundsException se o vértice 1 ou vértice 2 forem
     * negativos
     * @throws IllegalArgumentException se o peso for um valor não numérico
     */
    public Aresta(Vertice v1, Vertice v2, int km, int estado) {
        if (v1.getId() < 0) {
            throw new IndexOutOfBoundsException("Vértice deve ser um inteiro não negativo");
        }
        if (v2.getId() < 0) {
            throw new IndexOutOfBoundsException("Vértice deve ser um inteiro não negativo");
        }

        this.v1 = v1;
        this.v2 = v2;
        this.km = km;
        this.estado = estado;
    }

    public Vertice getV1() {
        return v1;
    }

    public Vertice getV2() {
        return v2;
    }

    public void setV1(Vertice v1) {
        this.v1 = v1;
    }

    public void setV2(Vertice v2) {
        this.v2 = v2;
    }

    public int getKm() {
        return km;
    }

    public int getEstado() {
        return estado;
    }

    public boolean getArestaByVertices(int valorVerticeInicial, int valorVerticeFinal) {
        boolean verticeInicial = valorVerticeInicial == this.v1.getId();
        boolean verticeFinal = valorVerticeFinal == this.v2.getId();

        return verticeInicial && verticeFinal;
    }

    /**
     * Retorna um vértice qualquer desta aresta (origem desta aresta).
     *
     * @return vértice 1 desta aresta
     */
    public Vertice umVertice() {
        return getV1();
    }

    /**
     * Retorna o outro vértice da aresta. Ou seja, o vértice diferente do
     * recebido como parâmetro.
     *
     * @param vertice um vértice qualquer desta aresta
     * @return o vértice diferente do recebido como parâmetro.
     * @throws IllegalArgumentException se o vértico do parâmetro não for um dos
     * vértices da aresta
     */
    public Vertice outroVertice(Vertice vertice) {
        if (vertice.getId() == getV1().getId()) {
            return getV2();
        } else if (vertice.getId() == getV2().getId()) {
            return getV1();
        } else {
            throw new IllegalArgumentException("Vértice inválido");
        }
    }

    /**
     * Compara o peso da aresta atual com o peso da aresta passada como
     * parâmetro.
     *
     * @param aquela a outra aresta
     * @return um inteiro negativo, zero ou um inteiro positivo, dependendo da
     * comparação
     */
    @Override
    public int compareTo(Aresta aquela) {
        // TODO: Criar comparação das arestas
        return 0;
//        return Double.compare(this.peso, aquela.peso);
    }

    /**
     * Retorna a representação String da aresta
     *
     * @return uma representação String da aresta
     */
    public String toString() {
        if (this.km != 0) {
            return String.format("%d-%d %d %d", getV1().getId(), getV2().getId(), getKm(), getEstado());
        } else {
            return String.format("%d", getV2());
        }
    }

}

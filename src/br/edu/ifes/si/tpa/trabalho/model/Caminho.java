/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author herik
 */
public class Caminho {

    List<Vertice> listVertices;
    List<Aresta> listArestas;
    Grafo grafo;
    Vertice vInicial;

    private double distanciaAtual;
    private double indice;
    private double iph;
    private double populacao;
    private String caminhoExibido;

    public Caminho(Grafo grafo, Vertice vInicial, List<Vertice> listVertices) {
        this.listVertices = new ArrayList<>();
        this.listArestas = new ArrayList<>();
        this.distanciaAtual = 0;

        this.grafo = grafo;
        this.vInicial = vInicial;

        this.addVertice(listVertices);
        this.montaCaminhoExibido();

        this.calculaDistancia();
        this.calculaIndices();
    }

    private void addVertice(List<Vertice> listVerticesInsercao) {
        this.listVertices.add(vInicial);

        for (Vertice v2 : listVerticesInsercao) {
            this.listVertices.add(v2);
            Vertice v1 = this.getVerticeAnterior();
            this.addAresta(v1, v2);
        }
    }

    private void addAresta(Vertice v1, Vertice v2) {
        for (Aresta aresta : grafo.arestas()) {
            if (aresta.getArestaByVertices(v1.getId(), v2.getId())) {
                this.addArestaLista(aresta);
            }
        }
    }

    private void addArestaLista(Aresta aresta) {
        if (!isArestaDuplicadas(aresta)) {
            this.listArestas.add(aresta);
        } else {
            this.distanciaAtual = Double.MAX_VALUE;
        }
    }

    private boolean isArestaDuplicadas(Aresta aresta) {
        for (Aresta arestaLista : listArestas) {
            if (comparaAresta1(aresta, arestaLista) && comparaAresta2(aresta, arestaLista)) {
                return true;
            }
        }
        return false;
    }

    private boolean comparaAresta1(Aresta a1, Aresta a2) {
        return a1.getV1().getId() == a2.getV2().getId();
    }

    private boolean comparaAresta2(Aresta a1, Aresta a2) {
        return a1.getV2().getId() == a2.getV1().getId();
    }

    private void calculaDistancia() {
        for (Aresta aresta : listArestas) {
            this.distanciaAtual += aresta.getKm();
        }
    }

    private Vertice getVerticeAnterior() {
        int tamanhoVertice = this.listVertices.size();
        return this.listVertices.get(tamanhoVertice - 2);
    }

    private void calculaIndices() {
        this.iph = this.calculaIPH();
        this.populacao = this.somaPopulacao();
        this.indice = this.iph * (this.populacao / 250000) * (this.distanciaAtual / 50);
    }

    private double calculaIPH() {
        double somatorioIPH = 0;

        for (Vertice vertice : listVertices) {
            somatorioIPH += vertice.getIph();
        }

        return somatorioIPH / listVertices.size();
    }

    private int somaPopulacao() {
        int populacaoTotal = 0;

        for (Vertice vertice : listVertices) {
            populacaoTotal += vertice.getPopulacao();
        }

        return populacaoTotal;
    }

    public void montaCaminhoExibido() {
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < listVertices.size(); i++) {
            str.append(String.valueOf(listVertices.get(i).getId()));

            if (i != listVertices.size() - 1) {
                str.append(" - ");
            }
        }

        this.caminhoExibido = str.toString();
    }

    public double getDistanciaAtual() {
        return distanciaAtual;
    }

    public double getIndice() {
        return indice;
    }

    public double getIph() {
        return iph;
    }

    public double getPopulacao() {
        return populacao;
    }

    public String getCaminhoExibido() {
        return caminhoExibido;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifes.si.tpa.trabalho.model;

/**
 *
 * @author herik
 */
public class Vertice {

    private int id;
    private int x;
    private int y;
    private int populacao;
    private double iph;

    public Vertice() {
    }

    public Vertice(int id, int x, int y, int populacao, double iph) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.populacao = populacao;
        this.iph = iph;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPopulacao() {
        return populacao;
    }

    public void setPopulacao(int populacao) {
        this.populacao = populacao;
    }

    public double getIph() {
        return iph;
    }

    public void setIph(double iph) {
        this.iph = iph;
    }

    @Override
    public String toString() {
        return "Vertice{" + "id=" + id + ", x=" + x + ", y=" + y + ", populacao=" + populacao + ", iph=" + iph + '}';
    }

}
